<?php
/*
Plugin Name: 40 Hadith of An Nawawi
Version: 1.0
Plugin URI: https://wordpress.org/plugins/hadith-an-nawawi/
Description: Display one random hadith from the 40 hadith in your widget area
Author: mmrs151
Author URI: http://mmrs151.tumblr.com/
*/

require_once('Assets/HadithDb.php');

class HadithAnNawawi extends WP_Widget
{
    /** @var HadithDb */
    private $hadithDb;

    public function __construct()
    {
        $this->hadithDb = new HadithDb();
        $this->add_stylesheet();
        $this->add_scripts();
        $widget_details = array(
            'className' => 'HadithAnNawawi',
            'description' => 'Display one random hadith from the 40 hadith in your widget area'
        );
        parent::__construct('HadithAnNawawi', '40 Hadith of An-Nawawi', $widget_details);
    }

    public function form($instance)
    {
        ?>
        <div xmlns="http://www.w3.org/1999/html">

            <span></br></br>

            </span>
        </div>

        <div class='mfc-text'>
        </div>

        <?php

        echo $args['after_widget'];
        echo "</br><a href='http://edgwareict.org.uk/' target='_blank'>Support EICT</a></br></br>";
    }

    public function widget($args, $instance)
    {
        echo $args['before_widget'];

        $quote = $this->hadithDb->getQuote();

        $blessing = $quote['narrator'] == 'Aisha' ? 'Radhiyallah Anha' : 'Radhiyallah Anh';

        $hadith = trim($quote['hadith']);
        if (strlen($hadith) > 200) {
            $hadith = substr($quote['hadith'], 0, 200) . '<a href="#ex1" rel="modal:open"> ...more</a>';
        }

        echo "
            <div class=\"randonHadeeth\">
                <h3 class=\"hadeethTitle\">". $quote['title'] ."<h3>
                <blockquote><p class=\"hadeethContent\">". $hadith ."</p></blockquote>
                <p class=\"hadeethNarrator\">". $quote['narrator'] ."
            </div>
            <p>". $blessing ."</p>
        ";

        echo "
          <!-- Modal HTML embedded directly into document -->
        <div id=\"ex1\" style=\"display:none;\">
            <p>". $quote['hadith'] ."</p>
        </div>";

        echo $args['after_widget'];
    }

    public function update( $new_instance, $old_instance ) {
        return $new_instance;
    }

    private function add_stylesheet() {
        wp_register_style( 'hadith-an-nawawi', plugins_url('Assets/styles.css', __FILE__) );
        wp_enqueue_style( 'hadith-an-nawawi' );
    }

    private function add_scripts() {
        // Get the Path to this plugin's folder
        $path = plugin_dir_url( __FILE__ );
        wp_enqueue_script( 'jquery-modal',
            $path. 'Assets/jquery.modal.js',
            array( 'jquery' ),
            '1.0.0', true );
    }
}

add_action('widgets_init', 'init_hadith_an_nawawi_widget');
function init_hadith_an_nawawi_widget()
{
    register_widget('HadithAnNawawi');
}

register_deactivation_hook( __FILE__, 'uninstallHadithAnNawawi' );
function uninstallHadithAnNawawi()
{
    global $wpdb;
    $table = $wpdb->prefix . "hadithAnNawawi";

    $wpdb->query("DROP TABLE IF EXISTS $table");
}
