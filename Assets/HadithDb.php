<?php
require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

class HadithDb
{

    /** @var string  */
    private $dbTable = "";

    /** @var string */
    private $tableName = "";

    public function __construct()
    {
        global $wpdb;

        $this->tableName = $wpdb->prefix . "hadithAnNawawi";
        $this->dbTable = DB_NAME . "." .$this->tableName;

        $this->createTableIfNotExist();
    }

    public function getQuote()
    {
        global $wpdb;

        $rand = rand(1,42);

        $sql = "SELECT * FROM " .$this->tableName. " WHERE hadithId = " .$rand. ";";

        $result = $wpdb->get_row($sql, ARRAY_A);

        return $result;
    }

    private function createTableIfNotExist()
    {
        global $wpdb;

        $charset_collate = $wpdb->get_charset_collate();

        $sql = "CREATE TABLE " . $this->dbTable. "(
                  hadithId int(11) NOT NULL AUTO_INCREMENT,
                  title varchar(256) DEFAULT NULL,
                  narrator varchar(256) DEFAULT NULL,
                  hadith text,
                  PRIMARY KEY (hadithId)
                ) $charset_collate;";


        $wpdb->get_var("SHOW TABLES LIKE '". $this->tableName . "'");
        if($wpdb->num_rows != 1) {
            dbDelta( $sql );
            $this->importCsv();
        }

    }

    private function importCsv()
    {
        global $wpdb;

        if (($handle = fopen(plugin_dir_path( __FILE__ ) . "40hadith.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, "|")) !== FALSE) {
                $sql = "INSERT INTO " . $this->tableName .
                       "  (title, narrator, hadith) 
                       values ('" .esc_sql($data[1]). "', '" .esc_sql($data[2]). "', '" .esc_sql($data[3]). "');";
                $wpdb->query($sql);
            }
        } else {
            echo 'no file';
        }
    }
}
